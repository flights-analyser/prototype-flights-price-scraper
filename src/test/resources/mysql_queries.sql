use flight_analysis;

select departure_date,return_date,currency,price_per_person, outbound_airline_name, outbound_duration, outbound_stops, outbound_departure_time, return_airline_name, return_stops, return_duration, return_departure_time
from flights
WHERE data_source ='cheap-flights'
AND (outbound_stops < 2 AND return_stops < 2)
AND (
	(timestampdiff(SECOND,outbound_departure_time,outbound_arrival_time) / 3600)<=15
	AND (timestampdiff(SECOND,return_departure_time,return_arrival_time) / 3600)<=15
)
group by departure_date, return_date
HAVING price_per_person < 500
ORDER BY departure_date ASC,price_per_person ASC;

select departure_date,return_date,currency,price_per_person, outbound_airline_name, outbound_duration, outbound_stops, outbound_departure_time, return_airline_name, return_stops, return_duration, return_departure_time
from flights
WHERE data_source ='sky-scanner'
AND (outbound_stops < 2 AND return_stops < 2)
AND (
	(timestampdiff(SECOND,outbound_departure_time,outbound_arrival_time) / 3600)<=15
	AND (timestampdiff(SECOND,return_departure_time,return_arrival_time) / 3600)<=15
)
group by departure_date, return_date
HAVING price_per_person < 500
ORDER BY departure_date ASC,price_per_person ASC;