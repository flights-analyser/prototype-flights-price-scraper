package com.zorbitt.flightpriceanalyser.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by Ryan on 2017/05/20.
 */
public class AnalysisResult {

    private WebSearchResult searchResult;
    private LocalDate departureDate;
    private LocalDate returnDate;
    private String source;

    public AnalysisResult(WebSearchResult searchResult){
        assert(searchResult != null);
        this.searchResult = searchResult;
    }

    public WebSearchResult getSearchResult(){
        return this.searchResult;
    }

    public int getTotalResults(){
        return this.searchResult.getFlights().size();
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
