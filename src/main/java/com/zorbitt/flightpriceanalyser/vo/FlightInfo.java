package com.zorbitt.flightpriceanalyser.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Ryan on 2017/05/20.
 */
public class FlightInfo implements java.io.Serializable, Comparable<FlightInfo>{

    String source;
    LocalDate departureDate;
    LocalDate returnDate;
    double pricePerPerson;
    boolean withReturn = false;
    String currency;
    String outboundAirlineName;
    int outboundStops;
    String outboundDepartureAirportName;
    String outboundDuration;
    int outboundDurationVal;
    LocalDateTime outboundDepartureTime;
    String outboundArrivalAirportName;
    LocalDateTime outboundArrivalTime;
    String returnAirlineName;
    int returnStops;
    String returnDuration;
    int returnDurationVal;
    String returnDepartureAirportName;
    LocalDateTime returnDepartureTime;
    String returnArrivalAirportName;
    LocalDateTime returnArrivalTime;

    public FlightInfo(){

    }
    public String getId(){
        StringBuilder keyBuilder = new StringBuilder(departureDate.format(DateTimeFormatter.ISO_DATE));
        keyBuilder.append("_");
        keyBuilder.append(source);keyBuilder.append("_");
        keyBuilder.append(returnDate.format(DateTimeFormatter.ISO_DATE));keyBuilder.append("_");
        keyBuilder.append(outboundDepartureTime.format(DateTimeFormatter.ISO_DATE_TIME));keyBuilder.append("_");
        keyBuilder.append(outboundArrivalTime.format(DateTimeFormatter.ISO_DATE_TIME));keyBuilder.append("_");
        keyBuilder.append(outboundAirlineName);keyBuilder.append("_");
        keyBuilder.append(outboundStops);keyBuilder.append("_");
        keyBuilder.append(returnDepartureTime.format(DateTimeFormatter.ISO_DATE_TIME));keyBuilder.append("_");
        keyBuilder.append(returnArrivalTime.format(DateTimeFormatter.ISO_DATE_TIME));keyBuilder.append("_");
        keyBuilder.append(returnAirlineName);keyBuilder.append("_");
        keyBuilder.append(returnStops);
        return keyBuilder.toString();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public double getPricePerPerson() {
        return pricePerPerson;
    }

    public void setPricePerPerson(double pricePerPerson) {
        this.pricePerPerson = pricePerPerson;
    }

    public boolean isWithReturn() {
        return withReturn;
    }

    public void setWithReturn(boolean withReturn) {
        this.withReturn = withReturn;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOutboundAirlineName() {
        return outboundAirlineName;
    }

    public void setOutboundAirlineName(String outboundAirlineName) {
        this.outboundAirlineName = outboundAirlineName;
    }

    public int getOutboundStops() {
        return outboundStops;
    }

    public void setOutboundStops(int outboundStops) {
        this.outboundStops = outboundStops;
    }

    public String getOutboundDepartureAirportName() {
        return outboundDepartureAirportName;
    }

    public void setOutboundDepartureAirportName(String outboundDepartureAirportName) {
        this.outboundDepartureAirportName = outboundDepartureAirportName;
    }

    public String getOutboundDuration() {
        return outboundDuration;
    }

    public void setOutboundDuration(String outboundDuration) {
        this.outboundDuration = outboundDuration;
    }

    public int getOutboundDurationVal() {
        return outboundDurationVal;
    }

    public void setOutboundDurationVal(int outboundDurationVal) {
        this.outboundDurationVal = outboundDurationVal;
    }

    public LocalDateTime getOutboundDepartureTime() {
        return outboundDepartureTime;
    }

    public void setOutboundDepartureTime(LocalDateTime outboundDepartureTime) {
        this.outboundDepartureTime = outboundDepartureTime;
    }

    public String getOutboundArrivalAirportName() {
        return outboundArrivalAirportName;
    }

    public void setOutboundArrivalAirportName(String outboundArrivalAirportName) {
        this.outboundArrivalAirportName = outboundArrivalAirportName;
    }

    public LocalDateTime getOutboundArrivalTime() {
        return outboundArrivalTime;
    }

    public void setOutboundArrivalTime(LocalDateTime outboundArrivalTime) {
        this.outboundArrivalTime = outboundArrivalTime;
    }

    public String getReturnAirlineName() {
        return returnAirlineName;
    }

    public void setReturnAirlineName(String returnAirlineName) {
        this.returnAirlineName = returnAirlineName;
    }

    public int getReturnStops() {
        return returnStops;
    }

    public void setReturnStops(int returnStops) {
        this.returnStops = returnStops;
    }

    public String getReturnDuration() {
        return returnDuration;
    }

    public void setReturnDuration(String returnDuration) {
        this.returnDuration = returnDuration;
    }

    public int getReturnDurationVal() {
        return returnDurationVal;
    }

    public void setReturnDurationVal(int returnDurationVal) {
        this.returnDurationVal = returnDurationVal;
    }

    public String getReturnDepartureAirportName() {
        return returnDepartureAirportName;
    }

    public void setReturnDepartureAirportName(String returnDepartureAirportName) {
        this.returnDepartureAirportName = returnDepartureAirportName;
    }

    public LocalDateTime getReturnDepartureTime() {
        return returnDepartureTime;
    }

    public void setReturnDepartureTime(LocalDateTime returnDepartureTime) {
        this.returnDepartureTime = returnDepartureTime;
    }

    public String getReturnArrivalAirportName() {
        return returnArrivalAirportName;
    }

    public void setReturnArrivalAirportName(String returnArrivalAirportName) {
        this.returnArrivalAirportName = returnArrivalAirportName;
    }

    public LocalDateTime getReturnArrivalTime() {
        return returnArrivalTime;
    }

    public void setReturnArrivalTime(LocalDateTime returnArrivalTime) {
        this.returnArrivalTime = returnArrivalTime;
    }

    public int compareTo(FlightInfo o) {
        if (this.pricePerPerson< o.getPricePerPerson()){
            return -1;
        } else if (this.pricePerPerson == o.getPricePerPerson()){
            try {
                if (this.getReturnDurationVal()+this.getOutboundDurationVal()> o.getReturnDurationVal()+o.getOutboundDurationVal()){
                    return -1;
                } else if (this.getReturnDurationVal()+this.getOutboundDurationVal()< o.getReturnDurationVal()+o.getOutboundDurationVal()){
                    return 1;
                }
                return 0;
            } catch (Exception e){
                e.printStackTrace();
                return 0;
            }
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return "FlightInfo{" +
                "departureDate="+(departureDate != null ? departureDate.format(DateTimeFormatter.BASIC_ISO_DATE) : "null") + '\'' +
                ", returnDate="+(returnDate != null ? returnDate.format(DateTimeFormatter.BASIC_ISO_DATE) : "null") + '\'' +
                ", pricePerPerson=" + pricePerPerson + '\'' +
                ", withReturn=" + withReturn + '\'' +
                ", currency='" + currency + '\'' +
                ", outboundAirlineName='" + outboundAirlineName + '\'' +
                ", outboundStops=" + outboundStops +
                ", outboundDepartureAirportName='" + outboundDepartureAirportName + '\'' +
                ", outboundDuration='" + outboundDuration + '\'' +
                ", outboundDurationVal=" + outboundDurationVal +
                ", outboundDepartureTime='" + (outboundDepartureTime != null ? outboundDepartureTime.format(DateTimeFormatter.ISO_DATE_TIME) : "null") + '\'' +
                ", outboundArrivalAirportName='" + outboundArrivalAirportName + '\'' +
                ", outboundArrivalTime='" + (outboundArrivalTime != null ? outboundArrivalTime.format(DateTimeFormatter.ISO_DATE_TIME) : "null") + '\'' +
                ", returnAirlineName='" + returnAirlineName + '\'' +
                ", returnStops=" + returnStops +
                ", returnDuration='" + returnDuration + '\'' +
                ", returnDurationVal=" + returnDurationVal +
                ", returnDepartureAirportName='" + returnDepartureAirportName + '\'' +
                ", returnDepartureTime='" + (returnDepartureTime != null ? returnDepartureTime.format(DateTimeFormatter.ISO_DATE_TIME) : "null") + '\'' +
                ", returnArrivalAirportName='" + returnArrivalAirportName + '\'' +
                ", returnArrivalTime='" + (returnArrivalTime != null ? returnArrivalTime.format(DateTimeFormatter.ISO_DATE_TIME) : "null") + '\'' +
                '}';
    }
}
