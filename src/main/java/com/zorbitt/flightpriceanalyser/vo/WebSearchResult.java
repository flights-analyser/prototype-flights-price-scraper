package com.zorbitt.flightpriceanalyser.vo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Ryan on 2017/05/20.
 */
public class WebSearchResult {

    private boolean successful = false;
    private List<FlightInfo> flights = new LinkedList<FlightInfo>();

    public WebSearchResult(boolean successful){
        this.successful = successful;
    }

    public boolean isSuccessful(){
        return this.successful;
    }

    public void addFlight(FlightInfo flight){
        flights.add(flight);
    }

    public void setFlights(List<FlightInfo> flights){
        this.flights.clear();;
        this.flights.addAll(flights);
    }
    public List<FlightInfo> getFlights(){
        return this.flights;
    }
}
