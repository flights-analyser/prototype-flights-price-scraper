package com.zorbitt.flightpriceanalyser.vo;

import org.openqa.selenium.WebDriver;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by Ryan on 2017/05/21.
 */
public class ScraperQuery {
    final WebDriver webDriver;
    final String url;
    final int pagesToScrape;
    final LocalDate departureDate;
    final LocalDate returnDate;

    public ScraperQuery(WebDriver webDriver, String url, int pagesToScrape, LocalDate departureDate, LocalDate returnDate){
        assert(webDriver != null);
        assert(url != null);
        assert(departureDate != null);
        assert(returnDate != null);
        this.webDriver = webDriver;
        this.url = url;
        this.pagesToScrape = pagesToScrape;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public String getUrl() {
        return url;
    }

    public int getPagesToScrape() {
        return pagesToScrape;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    @Override
    public String toString() {
        return "ScraperQuery{" +
                "webDriverClass=" + webDriver.getClass().getSimpleName() +
                ", url='" + url + '\'' +
                ", pagesToScrape=" + pagesToScrape +
                ", departureDate=" + departureDate.format(DateTimeFormatter.ISO_DATE) +
                ", returnDate=" + returnDate.format(DateTimeFormatter.ISO_DATE) +
                '}';
    }
}
