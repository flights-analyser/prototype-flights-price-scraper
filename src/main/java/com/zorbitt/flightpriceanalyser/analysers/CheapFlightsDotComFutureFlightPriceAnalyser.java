package com.zorbitt.flightpriceanalyser.analysers;

import com.zorbitt.flightpriceanalyser.dao.MongoFlightDAO;
import com.zorbitt.flightpriceanalyser.dao.MySQLFlightDAO;
import com.zorbitt.flightpriceanalyser.scraper.CheapFlightsDotComFlightScraper;
import com.zorbitt.flightpriceanalyser.selenium.WebDriverStore;
import com.zorbitt.flightpriceanalyser.vo.AnalysisResult;
import com.zorbitt.flightpriceanalyser.vo.ScraperQuery;
import com.zorbitt.flightpriceanalyser.vo.WebSearchResult;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ryan on 2017/05/20.
 */
public class CheapFlightsDotComFutureFlightPriceAnalyser implements FlightPriceAnalyser{
    private Logger LOGGER = LoggerFactory.getLogger(CheapFlightsDotComFutureFlightPriceAnalyser.class);
    private static String DATE_FORMAT = "yyyy-MM-dd";

    public List<AnalysisResult> analyse(String baseSearchURL, LocalDate departureDate, long tripDuration){
        long DAY_INCREMENT = 1;
        LocalDate returnDate = departureDate.plusDays(tripDuration);
        final int pagesToScrape=2;

        final CheapFlightsDotComFlightScraper cheapFlightsDotComFlightScraper = new CheapFlightsDotComFlightScraper();
        final int DAY_RANGE_LIMIT=60;
        final List<AnalysisResult> allAnalysis = new ArrayList<>();
        final MongoFlightDAO mongoFlightDAO = new MongoFlightDAO();
        final MySQLFlightDAO mySQLFlightDAO = new MySQLFlightDAO();
        int curRange=0;
        WebDriver webDriver = null;
        try {
            webDriver = WebDriverStore.getWebDriver(ChromeDriver.class);
            while (curRange < DAY_RANGE_LIMIT) {
                String searchURL = String.format(baseSearchURL, departureDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT)), returnDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT)));
                WebSearchResult searchResult = cheapFlightsDotComFlightScraper.scrapeURL(new ScraperQuery(webDriver, searchURL, pagesToScrape, departureDate, returnDate));
                AnalysisResult analysisResult = new AnalysisResult(searchResult);
                analysisResult.setSource("cheap-flights");
                if (!searchResult.isSuccessful()) {
                    LOGGER.info("Search {} unsuccessful. Not analysing data.", searchURL);
                    continue;
                }
                analyseByPriceAndDuration(analysisResult);
                analysisResult.setDepartureDate(departureDate);
                analysisResult.setReturnDate(returnDate);

                mongoFlightDAO.persistAnalysis(analysisResult);
                mySQLFlightDAO.persistAnalysis(analysisResult);
                allAnalysis.add(analysisResult);
                departureDate = departureDate.plusDays(DAY_INCREMENT);
                returnDate = returnDate.plusDays(tripDuration);
                curRange += 1;
            }
        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
        } finally {
            WebDriverStore.closeAllDrivers();
        }
        return allAnalysis;
    }


    private void analyseByPriceAndDuration(AnalysisResult analysisResult){
        Collections.sort(analysisResult.getSearchResult().getFlights());
    }
}