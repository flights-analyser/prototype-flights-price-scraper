package com.zorbitt.flightpriceanalyser.analysers;

import com.zorbitt.flightpriceanalyser.vo.AnalysisResult;
import com.zorbitt.flightpriceanalyser.vo.WebSearchResult;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Ryan on 2017/05/20.
 */
public interface FlightPriceAnalyser {
    List<AnalysisResult> analyse(String searchURL, LocalDate departureDate, long tripDuration);

}
