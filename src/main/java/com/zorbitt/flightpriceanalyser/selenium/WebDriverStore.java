package com.zorbitt.flightpriceanalyser.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Ryan on 2017/05/21.
 */
public class WebDriverStore {
    
    private static Logger LOGGER = LoggerFactory.getLogger(WebDriverStore.class);
    private static Map<String, WebDriver> webDriverMap = new ConcurrentHashMap<>();

    public static WebDriver getWebDriver(Class instanceType){
        if (instanceType == ChromeDriver.class){
            WebDriver driver = webDriverMap.get(instanceType.getSimpleName());
            if (driver == null) {
                driver = createChromeWebDriver();
                webDriverMap.put(instanceType.getSimpleName(), driver);
                return driver;
            }
        }
        return null;
    }

    private static ChromeDriver createChromeWebDriver() {
        URL chromeDriver = WebDriverStore.class.getResource("/lib/chromedriver.exe");

        System.setProperty("webdriver.chrome.driver", chromeDriver.getFile());
        return new ChromeDriver(DesiredCapabilities.chrome());
    }

    public static void closeAllDrivers(){
        for (WebDriver webDriver: webDriverMap.values()){
            try{
                webDriver.quit();
            } catch (Exception e){
                LOGGER.error(e.getMessage(),e);
            }
        }
    }
}
