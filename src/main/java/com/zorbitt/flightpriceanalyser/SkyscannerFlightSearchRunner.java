package com.zorbitt.flightpriceanalyser;

import com.zorbitt.flightpriceanalyser.analysers.FlightPriceAnalyser;
import com.zorbitt.flightpriceanalyser.analysers.SkyScannerFutureFlightPriceAnalyser;
import com.zorbitt.flightpriceanalyser.vo.AnalysisResult;
import com.zorbitt.flightpriceanalyser.vo.FlightInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ryan on 2017/05/20.
 */
public class SkyscannerFlightSearchRunner {
    private static Logger LOGGER = LoggerFactory.getLogger(SkyscannerFlightSearchRunner.class);

    public static void main(String[] args){
        final String departureCity="jnb";
        final String arrivalCity="ath";
        final String departureAirport="johannesburg-o.r.-tambo";
        final String arrivalAirport="athens-international";
        final String adults = "1";
        final String children = "0";
        final String withReturn = "1";

        StringBuilder baseURL = new StringBuilder("https://www.skyscanner.net/transport/flights/");
        baseURL.append(departureCity);
        baseURL.append("/");
        baseURL.append(arrivalCity);
        baseURL.append("/%s/%s/airfares-from-");
        baseURL.append(departureAirport);
        baseURL.append("-to-");
        baseURL.append(arrivalAirport);
        baseURL.append(".html?adults=");
        baseURL.append(adults);
        baseURL.append("&children=");
        baseURL.append(children);
        baseURL.append("&cabinclass=economy&rtn=");
        baseURL.append(withReturn);
        baseURL.append("&ref=home#results");

        final long tripDuration = 7;
        LocalDate departureDate = LocalDate.now();
        FlightPriceAnalyser flightPriceAnalyser = new SkyScannerFutureFlightPriceAnalyser();
        List<AnalysisResult> analysisResults = flightPriceAnalyser.analyse(baseURL.toString(), departureDate, tripDuration);
        List<AnalysisResult>  allAnalysis = new ArrayList();
        for (AnalysisResult analysisResult: analysisResults) {
            analyseByPriceAndDuration(analysisResult);
            allAnalysis.add(analysisResult);
        }
        for (AnalysisResult analysisResult: analysisResults) {
            for (FlightInfo flightInfo : analysisResult.getSearchResult().getFlights()) {
                LOGGER.info("Analyser {} Flight Info {} ", SkyscannerFlightSearchRunner.class.getSimpleName(), flightInfo);
            }
        }
    }

    private static void analyseByPriceAndDuration(AnalysisResult analysisResult){
        Collections.sort(analysisResult.getSearchResult().getFlights());
    }
}