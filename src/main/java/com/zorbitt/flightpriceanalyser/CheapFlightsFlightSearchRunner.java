package com.zorbitt.flightpriceanalyser;

import com.zorbitt.flightpriceanalyser.analysers.CheapFlightsDotComFutureFlightPriceAnalyser;
import com.zorbitt.flightpriceanalyser.analysers.FlightPriceAnalyser;
import com.zorbitt.flightpriceanalyser.vo.AnalysisResult;
import com.zorbitt.flightpriceanalyser.vo.FlightInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ryan on 2017/05/20.
 */
public class CheapFlightsFlightSearchRunner {
    private static Logger LOGGER = LoggerFactory.getLogger(CheapFlightsFlightSearchRunner.class);


    public static void main(String[] args){
        String fromLocation = "Or-Tambo";
        String arrivalLocation="Athens";
        StringBuilder baseURLBuilder = new StringBuilder("https://www.cheapflights.co.uk/search/flights-to-");
        baseURLBuilder.append(arrivalLocation);
        baseURLBuilder.append("/from-");
        baseURLBuilder.append(fromLocation);
        baseURLBuilder.append("/departing-%s/returning-%s/");
        long tripDuration = 7;
        LocalDateTime departureDate = LocalDateTime.now();

        FlightPriceAnalyser flightPriceAnalyser = new CheapFlightsDotComFutureFlightPriceAnalyser();
        List<AnalysisResult> analysisResults = flightPriceAnalyser.analyse(baseURLBuilder.toString(), departureDate.toLocalDate(), tripDuration);
        List<AnalysisResult>  allAnalysis = new ArrayList();
        for (AnalysisResult analysisResult: analysisResults) {
            analyseByPriceAndDuration(analysisResult);
            allAnalysis.add(analysisResult);
        }
        for (AnalysisResult analysisResult: analysisResults) {
            for (FlightInfo flightInfo : analysisResult.getSearchResult().getFlights()) {
                LOGGER.info("Analyser {} Flight Info {} ", CheapFlightsFlightSearchRunner.class.getSimpleName(), flightInfo);
            }
        }
    }

    private static void analyseByPriceAndDuration(AnalysisResult analysisResult){
        Collections.sort(analysisResult.getSearchResult().getFlights());
    }
}