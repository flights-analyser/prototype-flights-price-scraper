package com.zorbitt.flightpriceanalyser.dao;

public enum FlightInfoDBObject {

    ID("_id"),
    SOURCE("source"),
    RETURN_DATE("return_date"),
    DEPARTURE_DATE("departure_date"),
    CURRENCY("currency"),
    PRICE_PER_PERSON("price_per_person"),
    OUTBOUND_AIRLINE_NAME("outbound_airline_name"),
    OUTBOUND_DURATION("outbound_duration"),
    OUTBOUND_STOPS("outbound_stops"),
    OUTBOUND_DEPARTURE_TIME("outbound_departure_time"),
    OUTBOUND_DEPARTURE_AIRPORT_NAME("outbound_departure_airport_name"),
    OUTBOUND_ARRIVAL_TIME("outbound_arrival_time"),
    OUTBOUND_ARRIVAL_AIRPORT_NAME("outbound_arrival_airport_name"),
    RETURN_AIRLINE_NAME("return_airline_name"),
    RETURN_DURATION("return_duration"),
    RETURN_STOPS("return_stops"),
    RETURN_DEPARTURE_TIME("return_departure_time"),
    RETURN_DEPARTURE_AIRPORT_NAME("return_departure_airport_name"),
    RETURN_ARRIVAL_TIME("return_arrival_time"),
    RETURN_ARRIVAL_AIRPORT_NAME("return_arrival_airport_name");

    private String fieldKey;
    FlightInfoDBObject(String fieldKey){
        this.fieldKey=fieldKey;
    }

    public String getFieldKey() {
        return fieldKey;
    }
}
