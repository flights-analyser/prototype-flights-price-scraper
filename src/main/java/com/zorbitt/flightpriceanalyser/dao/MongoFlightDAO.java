package com.zorbitt.flightpriceanalyser.dao;

import com.mongodb.*;
import com.zorbitt.flightpriceanalyser.CheapFlightsFlightSearchRunner;
import com.zorbitt.flightpriceanalyser.util.PropertiesUtil;
import com.zorbitt.flightpriceanalyser.vo.AnalysisResult;
import com.zorbitt.flightpriceanalyser.vo.FlightInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;
import java.time.format.DateTimeFormatter;

/**
 * Created by Ryan on 2017/05/21.
 */
public class MongoFlightDAO {
    Logger LOGGER = LoggerFactory.getLogger(MongoFlightDAO.class);
    private PropertiesUtil propertiesUtil = new PropertiesUtil();

    public void persistAnalysis(AnalysisResult analysisResult){
        MongoClient mongo = getMongoClient();
        if (mongo == null){
            return;
        }
        try{
            String dbName = propertiesUtil.getProperty("mongo.db.name");
            String collectionName = propertiesUtil.getProperty("mongo.db.collection.name");
            DB analysisDatabase = mongo.getDB(dbName);

            DBCollection analysisCollection = analysisDatabase.getCollection(collectionName);
            for (FlightInfo flightInfo: analysisResult.getSearchResult().getFlights()) {
                flightInfo.setDepartureDate(analysisResult.getDepartureDate());
                flightInfo.setReturnDate(analysisResult.getReturnDate());
                flightInfo.setSource(analysisResult.getSource());

                DBObject entry = createDBObject(flightInfo);
                DBObject updateByKey = createDBObjectKey(flightInfo);

                analysisCollection.update(updateByKey, entry, true, false);
            }
        } catch (Exception e){
            LOGGER.error(e.getLocalizedMessage(), e);
        } finally {
            mongo.close();
        }
    }
    private DBObject createDBObjectKey(FlightInfo flightInfo){
        DBObject updateByKey = new BasicDBObject();
        updateByKey.put(FlightInfoDBObject.ID.getFieldKey(), flightInfo.getId());
        return updateByKey;
    }
    private DBObject createDBObject(FlightInfo flightInfo){
        DBObject entry = new BasicDBObject();
        entry.put(FlightInfoDBObject.ID.getFieldKey(), flightInfo.getId());
        entry.put(FlightInfoDBObject.SOURCE.getFieldKey(), flightInfo.getSource());
        entry.put(FlightInfoDBObject.DEPARTURE_DATE.getFieldKey(), flightInfo.getDepartureDate().format(DateTimeFormatter.ISO_DATE));
        entry.put(FlightInfoDBObject.RETURN_DATE.getFieldKey(), flightInfo.getReturnDate().format(DateTimeFormatter.ISO_DATE));
        entry.put(FlightInfoDBObject.CURRENCY.getFieldKey(), flightInfo.getCurrency());
        entry.put(FlightInfoDBObject.PRICE_PER_PERSON.getFieldKey(), flightInfo.getPricePerPerson());
        //outbound
        entry.put(FlightInfoDBObject.OUTBOUND_AIRLINE_NAME.getFieldKey(), flightInfo.getOutboundAirlineName());
        entry.put(FlightInfoDBObject.OUTBOUND_DURATION.getFieldKey(), flightInfo.getOutboundDuration());
        entry.put(FlightInfoDBObject.OUTBOUND_STOPS.getFieldKey(), flightInfo.getOutboundStops());
        entry.put(FlightInfoDBObject.OUTBOUND_DEPARTURE_TIME.getFieldKey(), flightInfo.getOutboundDepartureTime().format(DateTimeFormatter.ISO_DATE_TIME));
        entry.put(FlightInfoDBObject.OUTBOUND_DEPARTURE_AIRPORT_NAME.getFieldKey(), flightInfo.getOutboundDepartureAirportName());
        entry.put(FlightInfoDBObject.OUTBOUND_ARRIVAL_TIME.getFieldKey(), flightInfo.getOutboundArrivalTime().format(DateTimeFormatter.ISO_DATE_TIME));
        entry.put(FlightInfoDBObject.OUTBOUND_ARRIVAL_AIRPORT_NAME.getFieldKey(), flightInfo.getOutboundArrivalAirportName());
        //return
        entry.put(FlightInfoDBObject.RETURN_AIRLINE_NAME.getFieldKey(), flightInfo.getReturnAirlineName());
        entry.put(FlightInfoDBObject.RETURN_DURATION.getFieldKey(), flightInfo.getReturnDuration());
        entry.put(FlightInfoDBObject.RETURN_STOPS.getFieldKey(), flightInfo.getReturnStops());
        entry.put(FlightInfoDBObject.RETURN_DEPARTURE_TIME.getFieldKey(), flightInfo.getReturnDepartureTime().format(DateTimeFormatter.ISO_DATE_TIME));
        entry.put(FlightInfoDBObject.RETURN_DEPARTURE_AIRPORT_NAME.getFieldKey(), flightInfo.getReturnDepartureAirportName());
        entry.put(FlightInfoDBObject.RETURN_ARRIVAL_TIME.getFieldKey(), flightInfo.getReturnArrivalTime().format(DateTimeFormatter.ISO_DATE_TIME));
        entry.put(FlightInfoDBObject.RETURN_ARRIVAL_AIRPORT_NAME.getFieldKey(), flightInfo.getReturnArrivalAirportName());
        return entry;
    }

    private MongoClient getMongoClient(){
        MongoClient mongo = null;
        try {
            String host = propertiesUtil.getProperty("mongo.db.host");
            int port = Integer.parseInt(propertiesUtil.getProperty("mongo.db.port"));
            mongo = new MongoClient(host, port);
        } catch (UnknownHostException e) {
            LOGGER.error(e.getLocalizedMessage(),e);
        }
        return mongo;
    }
}
