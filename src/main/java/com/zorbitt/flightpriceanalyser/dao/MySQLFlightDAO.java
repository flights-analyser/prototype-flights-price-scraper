package com.zorbitt.flightpriceanalyser.dao;

import com.zorbitt.flightpriceanalyser.CheapFlightsFlightSearchRunner;
import com.zorbitt.flightpriceanalyser.util.PropertiesUtil;
import com.zorbitt.flightpriceanalyser.vo.AnalysisResult;
import com.zorbitt.flightpriceanalyser.vo.FlightInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.format.DateTimeFormatter;

/**
 * Created by Ryan on 2017/05/21.
 */
public class MySQLFlightDAO {

    Logger LOGGER = LoggerFactory.getLogger(MySQLFlightDAO.class);

    private PropertiesUtil propertiesUtil = new PropertiesUtil();

    public void persistAnalysis(AnalysisResult analysisResult){
        Connection connection = null;
        PreparedStatement insertStatement = null;
        PreparedStatement updateStatement = null;
        try{
            insertStatement = connection.prepareStatement(propertiesUtil.getProperty("mysql.flight.insert.statement"));
            updateStatement = connection.prepareStatement(propertiesUtil.getProperty("mysql.flight.update.statement"));
            int rowsAffected = 0;
            for (FlightInfo flightInfo: analysisResult.getSearchResult().getFlights()) {
                flightInfo.setDepartureDate(analysisResult.getDepartureDate());
                flightInfo.setReturnDate(analysisResult.getReturnDate());
                flightInfo.setSource(analysisResult.getSource());

                boolean update = flightExists(flightInfo.getId());

                if (update) {
                    assignUpdate(updateStatement, flightInfo);
                    rowsAffected += updateStatement.executeUpdate();
                    updateStatement.clearParameters();
                } else {
                    assignInsert(insertStatement, flightInfo);
                    rowsAffected += insertStatement.executeUpdate();
                    insertStatement.clearParameters();
                }
            }
            LOGGER.info("Inserted {} rows to MySQL", rowsAffected);
        } catch (Exception e){
            LOGGER.error(e.getLocalizedMessage(), e);
        } finally {
            try {
                if (connection != null){
                    if (insertStatement != null) {
                        insertStatement.close();
                    }
                    if (updateStatement != null) {
                        updateStatement.close();
                    }
                    connection.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getMessage(),e);
            }
        }
    }

    private void assignUpdate(PreparedStatement updateStatement, FlightInfo flightInfo) throws SQLException {
        int curParam = 0;
        updateStatement.setString(++curParam, flightInfo.getSource());
        updateStatement.setString(++curParam, flightInfo.getDepartureDate().format(DateTimeFormatter.ISO_DATE));
        updateStatement.setString(++curParam, flightInfo.getReturnDate().format(DateTimeFormatter.ISO_DATE));
        updateStatement.setString(++curParam, flightInfo.getCurrency());
        updateStatement.setDouble(++curParam, flightInfo.getPricePerPerson());
        //outbound
        updateStatement.setString(++curParam, flightInfo.getOutboundAirlineName());
        updateStatement.setString(++curParam, flightInfo.getOutboundDuration());
        updateStatement.setInt(++curParam, flightInfo.getOutboundStops());
        updateStatement.setString(++curParam, flightInfo.getOutboundDepartureTime().format(DateTimeFormatter.ISO_DATE_TIME));
        updateStatement.setString(++curParam, flightInfo.getOutboundDepartureAirportName());
        updateStatement.setString(++curParam, flightInfo.getOutboundArrivalTime().format(DateTimeFormatter.ISO_DATE_TIME));
        updateStatement.setString(++curParam, flightInfo.getOutboundArrivalAirportName());
        //return
        updateStatement.setString(++curParam, flightInfo.getReturnAirlineName());
        updateStatement.setString(++curParam, flightInfo.getReturnDuration());
        updateStatement.setInt(++curParam, flightInfo.getReturnStops());
        updateStatement.setString(++curParam, flightInfo.getReturnDepartureTime().format(DateTimeFormatter.ISO_DATE_TIME));
        updateStatement.setString(++curParam, flightInfo.getReturnDepartureAirportName());
        updateStatement.setString(++curParam, flightInfo.getReturnArrivalTime().format(DateTimeFormatter.ISO_DATE_TIME));
        updateStatement.setString(++curParam, flightInfo.getReturnArrivalAirportName());
        updateStatement.setString(++curParam, flightInfo.getId());
    }

    private void assignInsert(PreparedStatement insertStatement, FlightInfo flightInfo) throws SQLException {
        int curParam = 0;
        insertStatement.setString(++curParam, flightInfo.getId());
        insertStatement.setString(++curParam, flightInfo.getSource());
        insertStatement.setString(++curParam, flightInfo.getDepartureDate().format(DateTimeFormatter.ISO_DATE));
        insertStatement.setString(++curParam, flightInfo.getReturnDate().format(DateTimeFormatter.ISO_DATE));
        insertStatement.setString(++curParam, flightInfo.getCurrency());
        insertStatement.setDouble(++curParam, flightInfo.getPricePerPerson());
        //outbound
        insertStatement.setString(++curParam, flightInfo.getOutboundAirlineName());
        insertStatement.setString(++curParam, flightInfo.getOutboundDuration());
        insertStatement.setInt(++curParam, flightInfo.getOutboundStops());
        insertStatement.setString(++curParam, flightInfo.getOutboundDepartureTime().format(DateTimeFormatter.ISO_DATE_TIME));
        insertStatement.setString(++curParam, flightInfo.getOutboundDepartureAirportName());
        insertStatement.setString(++curParam, flightInfo.getOutboundArrivalTime().format(DateTimeFormatter.ISO_DATE_TIME));
        insertStatement.setString(++curParam, flightInfo.getOutboundArrivalAirportName());
        //return
        insertStatement.setString(++curParam, flightInfo.getReturnAirlineName());
        insertStatement.setString(++curParam, flightInfo.getReturnDuration());
        insertStatement.setInt(++curParam, flightInfo.getReturnStops());
        insertStatement.setString(++curParam, flightInfo.getReturnDepartureTime().format(DateTimeFormatter.ISO_DATE_TIME));
        insertStatement.setString(++curParam, flightInfo.getReturnDepartureAirportName());
        insertStatement.setString(++curParam, flightInfo.getReturnArrivalTime().format(DateTimeFormatter.ISO_DATE_TIME));
        insertStatement.setString(++curParam, flightInfo.getReturnArrivalAirportName());
    }

    private boolean flightExists(String flightId){
        Connection connection = null;
        PreparedStatement findStatement=null;
        try{
            connection = getConnection();
            findStatement = connection.prepareStatement(propertiesUtil.getProperty("mysql.flight.find.statement"));
            findStatement.setString(1, flightId);
            ResultSet resultSet = findStatement.executeQuery();
            resultSet.first();
            String ID = resultSet.getString("_id");
            if (ID.equalsIgnoreCase(flightId)){
                return true;
            }
        } catch (Exception e){
            LOGGER.error(e.getLocalizedMessage(), e);
        } finally {
            try {
                if (connection != null){
                    if (findStatement != null) {
                        findStatement.close();
                    }
                    if (findStatement != null) {
                        findStatement.close();
                    }
                    connection.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getMessage(),e);
            }
        }
        return false;
    }

    private Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String host = propertiesUtil.getProperty("mysql.db.host");
            String port = propertiesUtil.getProperty("mysql.db.port");
            String dbName = propertiesUtil.getProperty("mysql.db.name");
            String username = propertiesUtil.getProperty("mysql.db.username");
            String password = propertiesUtil.getProperty("mysql.db.password");
            final String connectionURLTemplate = "jdbc:mysql://%s:%s/%s";
            String connectionURL = String.format(connectionURLTemplate, host, port, dbName);
            connection = DriverManager.getConnection(connectionURL, username, password);
        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
        }
        return connection;
    }

}
