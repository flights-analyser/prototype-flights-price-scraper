package com.zorbitt.flightpriceanalyser.scraper;

import com.zorbitt.flightpriceanalyser.vo.ScraperQuery;
import com.zorbitt.flightpriceanalyser.vo.WebSearchResult;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Ryan on 2017/05/20.
 */
public interface FlightScraper {
    WebSearchResult scrapeURL(ScraperQuery scraperQuery);
    WebDriverWait createWebDriverWait(WebDriver webDriver);
}
