package com.zorbitt.flightpriceanalyser.scraper;

import com.zorbitt.flightpriceanalyser.vo.FlightInfo;
import com.zorbitt.flightpriceanalyser.vo.ScraperQuery;
import com.zorbitt.flightpriceanalyser.vo.WebSearchResult;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by Ryan on 2017/05/20.
 */
public class CheapFlightsDotComFlightScraper implements FlightScraper {
    private Logger LOGGER = LoggerFactory.getLogger(CheapFlightsDotComFlightScraper.class);
    private int MAX_PAGE_CHANGE = 2;
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    private static final String TIME_FORMAT = "HH:mm";
    public WebSearchResult scrapeURL(ScraperQuery scraperQuery) {
        if (scraperQuery.getPagesToScrape() > 0){
            MAX_PAGE_CHANGE = scraperQuery.getPagesToScrape();
        }
        try {
            LOGGER.info("Going to search {}", scraperQuery.getUrl());
            scraperQuery.getWebDriver().manage().window().maximize();
            WebDriverWait webDriverWait = createWebDriverWait(scraperQuery.getWebDriver());

            scraperQuery.getWebDriver().get(scraperQuery.getUrl());
            // Wait for search to complete
            webDriverWait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    LOGGER.info("Searching ...");
                    WebElement progressElement = webDriver.findElement(By.className("moreButton"));
                    if (progressElement != null){
                        return webDriver.findElement(By.className("moreButton")).isEnabled();
                    }
                    return false;
                }
            });
            LOGGER.info("Got Response");
            return extractFlightInfo(scraperQuery);
        } catch(Exception e) {
            LOGGER.error(e.getLocalizedMessage(),e);
            return new WebSearchResult(false);
        }

    }

    private WebSearchResult extractFlightInfo(ScraperQuery scraperQuery){
        WebSearchResult searchResult = null;
        WebDriver webDriver = scraperQuery.getWebDriver();
        WebElement leadingFlightResultElement = webDriver.findElement(By.className("Flights-Results-FlightResultItem"));
        if (leadingFlightResultElement != null){
            searchResult = new WebSearchResult(true);
        } else {
            searchResult = new WebSearchResult(false);
        }
        JavascriptExecutor js = (JavascriptExecutor)webDriver;
        final String executeScrollToEndOfPage = "window.scrollTo(0,document.body.scrollHeight);";
        js.executeScript(executeScrollToEndOfPage);
        if(webDriver.findElement(By.className("moreButton")).isDisplayed()){
            LOGGER.info("Will Search through maximum of Flights {} ", MAX_PAGE_CHANGE);
            webDriver.findElement(By.className("moreButton")).click();
            for (int currentPage = 2 ; currentPage<=MAX_PAGE_CHANGE; currentPage++){

                js.executeScript(executeScrollToEndOfPage);
                WebDriverWait nextPageButtonVisibleWait = new WebDriverWait(webDriver, 60);
                nextPageButtonVisibleWait.until(new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver webDriver) {
                        String paginatorClass = webDriver.findElement(By.className("resultsPaginator")).findElement(By.className("Common-Results-Paginator")).getAttribute("class");
                        if (paginatorClass.endsWith("visible")){
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
                String paginatorClass = webDriver.findElement(By.className("resultsPaginator")).findElement(By.className("Common-Results-Paginator")).getAttribute("class");
                if (paginatorClass.endsWith("ButtonPaginator")){
                    LOGGER.debug("No more results");
                    break;
                }
            }

            try {
                List<WebElement> flights = webDriver.findElements(By.className("Flights-Results-FlightResultItem"));
                for (WebElement element: flights){
                    searchResult.addFlight(getFlightInfo(element, scraperQuery));
                }
                LOGGER.info("Found {} flights", flights.size());
            } catch (WebDriverException e){
                LOGGER.error(e.getMessage());
            }
        } else {
            LOGGER.debug("No more results");
        }

        return searchResult;
    }

    private FlightInfo getFlightInfo(WebElement flightInfo, ScraperQuery scraperQuery){
        FlightInfo flightInfoParsed = new FlightInfo();
        flightInfoParsed.setDepartureDate(scraperQuery.getDepartureDate());
        flightInfoParsed.setReturnDate(scraperQuery.getReturnDate());
        List<WebElement> flights = flightInfo.findElements(By.className("Flights-Results-LegInfo"));
        if (flights.size()>2){
            LOGGER.debug("Too many legs to parse");
        } else {
            WebElement pricePerPerson = flightInfo.findElement(By.className("price"));
            String priceWithCurrency = pricePerPerson.getText();
            if (priceWithCurrency.equalsIgnoreCase("info")){
                flightInfoParsed.setPricePerPerson(Double.valueOf("0.0"));
            } else {
                flightInfoParsed.setPricePerPerson(Double.parseDouble(StringUtils.replace(StringUtils.substring(priceWithCurrency, 1),",","")));
            }


            flightInfoParsed.setWithReturn(flights.size()>1);

            flightInfoParsed.setCurrency(StringUtils.substring(priceWithCurrency, 0, 1));



            WebElement outboundSegment = flights.get(0);
            parseOutboundFlightSegment(outboundSegment, flightInfoParsed);
            if (flightInfoParsed.isWithReturn()) {
                WebElement returnSegment = flights.get(1);
                parseReturnFlightSegment(returnSegment, flightInfoParsed);
            }
        }
        return flightInfoParsed;
    }

    private void parseOutboundFlightSegment(WebElement outboundSegment, FlightInfo flightInfoParsed){
        flightInfoParsed.setOutboundAirlineName(outboundSegment.findElement(By.className("carrier")).findElement(By.className("bottom")).getText());
        flightInfoParsed.setOutboundDuration(outboundSegment.findElement(By.className("duration")).findElement(By.className("top")).getText());
        String[] outboundDurationParts = flightInfoParsed.getOutboundDuration().replace("h", "").replace("m", "").split(" ");
        int outboundDuration = Integer.parseInt(outboundDurationParts[0]) + Integer.parseInt(outboundDurationParts[1]);
        flightInfoParsed.setOutboundDurationVal(outboundDuration);

        WebElement outboundDepartureSegment = outboundSegment.findElement(By.className("depart"));

        flightInfoParsed.setOutboundDepartureAirportName(outboundDepartureSegment.findElement(By.className("bottom")).getText());
        flightInfoParsed.setOutboundDepartureTime(LocalTime.parse(outboundDepartureSegment.findElement(By.className("top")).getText(), DateTimeFormatter.ofPattern(TIME_FORMAT)).atDate(flightInfoParsed.getDepartureDate()));

        WebElement outboundArrivalSegment = outboundSegment.findElement(By.className("return"));
        String arrivalAirport = outboundArrivalSegment.findElement(By.className("bottom")).getText();
        String[] airportParts = StringUtils.split(arrivalAirport, "(");
        Long arrivalDaysDifference = 0L;
        if (airportParts != null && airportParts.length>1) {
            flightInfoParsed.setOutboundArrivalAirportName(airportParts[0]);
            String arrivalDaysDiffStr = StringUtils.remove(airportParts[1], ")");
            if (StringUtils.startsWith(arrivalDaysDiffStr, "+")) {
                arrivalDaysDifference += Integer.parseInt(arrivalDaysDiffStr);
            } else {
                arrivalDaysDifference -= Integer.parseInt(arrivalDaysDiffStr);
            }
        }else {
            flightInfoParsed.setOutboundArrivalAirportName(arrivalAirport);
        }
        flightInfoParsed.setOutboundArrivalTime(LocalTime.parse(outboundArrivalSegment.findElement(By.className("top")).getText(), DateTimeFormatter.ofPattern(TIME_FORMAT)).atDate(flightInfoParsed.getDepartureDate().plusDays(arrivalDaysDifference)));

        WebElement outboundStops = outboundSegment.findElement(By.className("stops"));
        if (outboundStops != null) {
            int stops = outboundStops.findElements(By.className("dot")).size();
            flightInfoParsed.setOutboundStops(stops);
        }
    }
    private void parseReturnFlightSegment(WebElement returnSegment, FlightInfo flightInfoParsed){
        flightInfoParsed.setReturnAirlineName(returnSegment.findElement(By.className("carrier")).findElement(By.className("bottom")).getText());
        flightInfoParsed.setReturnDuration(returnSegment.findElement(By.className("duration")).findElement(By.className("top")).getText());
        String[] returnDurationParts = flightInfoParsed.getReturnDuration().replace("h", "").replace("m", "").split(" ");
        int returnDuration = Integer.parseInt(returnDurationParts[0]) + Integer.parseInt(returnDurationParts[1]);
        flightInfoParsed.setReturnDurationVal(returnDuration);

        WebElement returnDepartureSegment = returnSegment.findElement(By.className("depart"));

        flightInfoParsed.setReturnDepartureAirportName(returnDepartureSegment.findElement(By.className("bottom")).getText());
        flightInfoParsed.setReturnDepartureTime(LocalTime.parse(returnDepartureSegment.findElement(By.className("top")).getText(), DateTimeFormatter.ofPattern(TIME_FORMAT)).atDate(flightInfoParsed.getReturnDate()));

        WebElement returnArrivalSegment = returnSegment.findElement(By.className("return"));
        String arrivalAirport = returnArrivalSegment.findElement(By.className("bottom")).getText();
        String[] airportParts = StringUtils.split(arrivalAirport, "(");
        Long arrivalDaysDifference = 0L;
        if (airportParts != null && airportParts.length>1) {
            flightInfoParsed.setReturnArrivalAirportName(airportParts[0]);
            String arrivalDaysDiffStr = StringUtils.remove(airportParts[1], ")");
            if (StringUtils.startsWith(arrivalDaysDiffStr, "+")) {
                arrivalDaysDifference += Integer.parseInt(arrivalDaysDiffStr);
            } else {
                arrivalDaysDifference -= Integer.parseInt(arrivalDaysDiffStr);
            }
        }else {
            flightInfoParsed.setReturnArrivalAirportName(arrivalAirport);
        }
        flightInfoParsed.setReturnArrivalTime(LocalTime.parse(returnArrivalSegment.findElement(By.className("top")).getText(), DateTimeFormatter.ofPattern(TIME_FORMAT)).atDate(flightInfoParsed.getReturnDate().plusDays(arrivalDaysDifference)));

        WebElement returnStops = returnSegment.findElement(By.className("stops"));
        if (returnStops != null) {
            int stops = returnStops.findElements(By.className("dot")).size();
            flightInfoParsed.setReturnStops(stops);
        }
    }
    public WebDriverWait createWebDriverWait(WebDriver webDriver) {
        return new WebDriverWait(webDriver, 60);
    }
}
