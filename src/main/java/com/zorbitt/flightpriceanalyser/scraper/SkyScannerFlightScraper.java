package com.zorbitt.flightpriceanalyser.scraper;

import com.zorbitt.flightpriceanalyser.vo.FlightInfo;
import com.zorbitt.flightpriceanalyser.vo.ScraperQuery;
import com.zorbitt.flightpriceanalyser.vo.WebSearchResult;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Sky Scanner are very clever, their website is difficult to scrape
 * Created by Ryan on 2017/05/21.
 */
public class SkyScannerFlightScraper implements FlightScraper {
    private Logger LOGGER = LoggerFactory.getLogger(SkyScannerFlightScraper.class);
    private int MAX_PAGE_CHANGE = 2;
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

    public WebSearchResult scrapeURL(ScraperQuery scraperQuery) {
        if (scraperQuery.getPagesToScrape() > 0){
            MAX_PAGE_CHANGE = scraperQuery.getPagesToScrape();
        }
        try {
            LOGGER.info("Going to search {}", scraperQuery.getUrl());
            scraperQuery.getWebDriver().manage().window().maximize();
            WebDriverWait webDriverWait = createWebDriverWait(scraperQuery.getWebDriver());

            scraperQuery.getWebDriver().get(scraperQuery.getUrl());

            // Wait for search to complete
            webDriverWait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    LOGGER.info("Searching ...");
                    try {
                        WebElement applicationBlocked = webDriver.findElement(By.className("block-page"));
                        if (applicationBlocked != null) {
                            applicationBlocked.isDisplayed();
                            return true;
                        }
                    } catch (NoSuchElementException e){

                    }
                    WebElement progressElement = webDriver.findElement(By.className("day-list-progress"));
                    if (progressElement != null){
                        String searchProgressStyle = progressElement.getAttribute("style");
                        if (searchProgressStyle.contains("display: none;")){
                            return true;
                        }
                    }
                    return false;
                }
            });
            LOGGER.info("Got Response");
            try {
                WebElement applicationBlocked = scraperQuery.getWebDriver().findElement(By.className("block-page"));
                if (applicationBlocked != null) {
                    applicationBlocked.isDisplayed();
                    LOGGER.error("Sky-scanner has blocked our scraping. Cant scrape data.");
                    return new WebSearchResult(false);
                }
            } catch (NoSuchElementException e){
                //no problem
            }
            return extractFlightInfo(scraperQuery.getWebDriver(), scraperQuery.getDepartureDate(), scraperQuery.getReturnDate());
        } catch(Exception e) {
            LOGGER.error(e.getLocalizedMessage(),e);
            return new WebSearchResult(false);
        }

    }

    private WebSearchResult extractFlightInfo(WebDriver webDriver, LocalDate departureDate, LocalDate returnDate){
        WebSearchResult searchResult = null;
        WebElement leadingFlightResultElement = webDriver.findElement(By.className("day-list"));
        if (leadingFlightResultElement != null){
            searchResult = new WebSearchResult(true);
        } else {
            searchResult = new WebSearchResult(false);
        }

        List<WebElement> pageNumberElements = webDriver.findElement(By.className("day-list-pagination")).findElement(By.className("clearfix")).findElements(By.tagName("li"));
        int totalPages = Integer.parseInt(pageNumberElements.get(pageNumberElements.size() >=2 ? pageNumberElements.size()-2: 0).getText());

        if (totalPages > MAX_PAGE_CHANGE) {
            totalPages = MAX_PAGE_CHANGE;
        }
        LOGGER.info("Total Pages of Flights {} ", totalPages);
        for (int currentPage = 1 ; currentPage<=totalPages; currentPage++){
            try {
                List<WebElement> flights = webDriver.findElements(By.className("day-list-item"));
                for (WebElement element: flights){
                    WebElement priceInsightPopup = webDriver.findElement(By.className("fss-popup"));
                    if (priceInsightPopup != null) {
                        Thread.sleep(3000);
                        if (priceInsightPopup.isDisplayed()) {
                            priceInsightPopup.findElement(By.className("close-panel")).click();
                            LOGGER.info("Closed Pop up blocking GUI");
                        }
                    }
                    searchResult.addFlight(getFlightInfo(element, departureDate, returnDate));
                    Thread.sleep(10000);
                }
                LOGGER.info("Found {} flights on Page {} ", flights.size(), currentPage);
                WebDriverWait nextPageButtonVisibleWait = new WebDriverWait(webDriver, 60);
                nextPageButtonVisibleWait.until(new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver webDriver) {
                        WebElement nextPageButton = webDriver.findElement(By.className("day-list-pagination")).findElement(By.className("next"));
                        if (nextPageButton.isDisplayed() && nextPageButton.isEnabled()) {
                            return true;
                        }
                        return false;
                    }
                });
                JavascriptExecutor js = (JavascriptExecutor)webDriver;
                js.executeScript("scroll(0, 4200);");
                webDriver.findElement(By.className("day-list-pagination")).findElement(By.className("next")).click();
                WebDriverWait nextPageAvailableWait = new WebDriverWait(webDriver, 60);
                final int finalCurrentPage = currentPage;
                nextPageAvailableWait.until(new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver webDriver) {
                        if ((finalCurrentPage+1) == Integer.parseInt(webDriver.findElement(By.className("day-list-pagination")).findElement(By.className("selected")).getText())) {
                            return true;
                        }
                        return false;
                    }
                });
                Thread.sleep(10000);
            } catch (WebDriverException e){
                LOGGER.error(e.getMessage());
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
            }
        }

        return searchResult;
    }

    private FlightInfo getFlightInfo(WebElement flightInfo, LocalDate departureDate, LocalDate returnDate){

        FlightInfo flightInfoParsed = new FlightInfo();
        flightInfoParsed.setSource("sky-scanner");
        flightInfoParsed.setDepartureDate(departureDate);
        flightInfoParsed.setReturnDate(returnDate);
        WebElement pricePerPerson = flightInfo.findElement(By.className("mainquote-group-price"));
        flightInfoParsed.setPricePerPerson(Double.parseDouble(pricePerPerson.getText().substring(1,pricePerPerson.getText().length())));
        flightInfoParsed.setCurrency(pricePerPerson.getText().substring(0,1));


        List<WebElement> flightLegs = flightInfo.findElement(By.className("card-body")).findElements(By.className("leg-details"));
        if (flightLegs.size()>1) {
            flightInfoParsed.setWithReturn(true);
        } else {
            flightInfoParsed.setWithReturn(false);
        }

        WebElement outboundDetails = flightLegs.get(0);

        flightInfoParsed.setOutboundAirlineName(flightInfo.findElement(By.className("big-airline")).findElement(By.className("big")).getAttribute("alt"));
        WebElement outboundDeparture = outboundDetails.findElement(By.className("depart"));
        flightInfoParsed.setOutboundDepartureTime(LocalDateTime.parse(departureDate.format(DateTimeFormatter.ISO_DATE)+ " " + outboundDeparture.findElement(By.className("times")).getText(), DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)));
        WebElement outboundArrival = outboundDetails.findElement(By.className("arrive")).findElement(By.className("times"));
        WebElement outboundDaysAddition = outboundArrival.findElement(By.tagName("small"));
        String outboundArrivalText = outboundArrival.getText();
        int outboundDaysAdditionVal = 0;
        if (outboundDaysAddition != null){
            outboundArrivalText = outboundArrivalText.replace(outboundDaysAddition.getText(),"");
            outboundArrivalText = StringUtils.remove(outboundArrivalText, '\n');
            outboundDaysAdditionVal = Integer.parseInt(outboundDaysAddition.getText().replace("(","").replace(")",""));
        }
        LocalDateTime outboundArrivalTime = LocalDateTime.parse(departureDate.format(DateTimeFormatter.ISO_DATE)+ " " + outboundArrivalText, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
        outboundArrivalTime.plusDays(outboundDaysAdditionVal);
        flightInfoParsed.setOutboundArrivalTime(outboundArrivalTime);
        flightInfoParsed.setOutboundDuration(outboundDetails.findElement(By.className("duration")).getText());
        String[] outboundDurationParts = flightInfoParsed.getOutboundDuration().replace("h", "").replace("m", "").split(" ");
        int outboundDuration;
        if (outboundDurationParts.length>1) {
            outboundDuration = Integer.parseInt(outboundDurationParts[0]) + Integer.parseInt(outboundDurationParts[1]);
        } else {
            outboundDuration = Integer.parseInt(outboundDurationParts[0]);

        }

        flightInfoParsed.setOutboundDurationVal(outboundDuration);
        WebElement outboundStops = outboundDetails.findElement(By.className("leg-stops-red"));
        if (outboundStops != null) {
            flightInfoParsed.setOutboundStops(Integer.parseInt(outboundStops.getText().replace(" stops", "").replace(" stop", "")));
        }
        flightInfoParsed.setOutboundDepartureAirportName("");
        flightInfoParsed.setOutboundArrivalAirportName("");
        if (flightInfoParsed.isWithReturn()){
            WebElement returnDetails = flightLegs.get(1);

            flightInfoParsed.setReturnAirlineName(flightInfo.findElement(By.className("big-airline")).findElement(By.className("big")).getAttribute("alt"));
            WebElement returnDeparture = returnDetails.findElement(By.className("depart"));
            flightInfoParsed.setReturnDepartureTime(LocalDateTime.parse(returnDate.format(DateTimeFormatter.ISO_DATE)+ " " + returnDeparture.findElement(By.className("times")).getText(), DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)));
            WebElement returnArrival = returnDetails.findElement(By.className("arrive")).findElement(By.className("times"));
            WebElement returnDaysAddition = returnArrival.findElement(By.tagName("small"));
            String returnArrivalText = returnArrival.getText();
            int returnDaysAdditionVal = 0;
            if (returnDaysAddition != null){
                returnArrivalText = returnArrivalText.replace(returnDaysAddition.getText(),"");
                returnArrivalText = StringUtils.remove(returnArrivalText, '\n');
                returnDaysAdditionVal = Integer.parseInt(returnDaysAddition.getText().replace("(","").replace(")",""));
            }
            LocalDateTime returnArrivalTime = LocalDateTime.parse(returnDate.format(DateTimeFormatter.ISO_DATE)+ " " + returnArrivalText, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
            returnArrivalTime.plusDays(returnDaysAdditionVal);
            flightInfoParsed.setReturnArrivalTime(returnArrivalTime);

            flightInfoParsed.setReturnDuration(returnDetails.findElement(By.className("duration")).getText());
            String[] returnDurationParts = flightInfoParsed.getReturnDuration().replace("h", "").replace("m", "").split(" ");
            int returnDuration;
            if (returnDurationParts.length>1) {
                returnDuration = Integer.parseInt(returnDurationParts[0]) + Integer.parseInt(returnDurationParts[1]);
            } else {
                returnDuration = Integer.parseInt(returnDurationParts[0]);

            }
            flightInfoParsed.setReturnDurationVal(returnDuration);
            WebElement returnStops = returnDetails.findElement(By.className("leg-stops-red"));
            if (returnStops != null) {
                flightInfoParsed.setReturnStops(Integer.parseInt(returnStops.getText().replace(" stops", "").replace(" stop", "")));
            }
            flightInfoParsed.setReturnDepartureAirportName("");
            flightInfoParsed.setReturnArrivalAirportName("");
        }

        return flightInfoParsed;
    }

    public WebDriverWait createWebDriverWait(WebDriver webDriver) {
        return new WebDriverWait(webDriver, 120);
    }
}
