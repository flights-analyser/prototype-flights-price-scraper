package com.zorbitt.flightpriceanalyser.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
    private String propertiesFile = "/application.properties";
    private Logger LOGGER = LoggerFactory.getLogger(PropertiesUtil.class);

    public PropertiesUtil(String propertiesFile){
        this.propertiesFile = propertiesFile;
    }
    public PropertiesUtil(){

    }

    public String getProperty(String propertyName){
        Properties props = getProperties(propertiesFile);
        return props.getProperty(propertyName);
    }

    private Properties getProperties(String propertyFileName){
        Properties props = new Properties();
        try {
            InputStream propsStream = this.getClass().getResourceAsStream(propertyFileName);
            if (propsStream != null) {
                props.load(propsStream);
            } else {
                propsStream = this.getClass().getResource(propertyFileName).openStream();
                if (propsStream != null) {
                    props.load(propsStream);
                }
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(),e);
        }
        return props;
    }
}
