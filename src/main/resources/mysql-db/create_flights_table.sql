use flight_analysis;

create table flights(
	_id varchar(512),
    data_source varchar(256),
    departure_date date,
    return_date date,
    currency varchar(1),
    price_per_person double,
    outbound_airline_name varchar(256),
    outbound_duration varchar(32),
    outbound_stops int,
    outbound_departure_time datetime,
    outbound_departure_airport varchar(256),
    outbound_arrival_time datetime,
    outbound_arrival_airport varchar(256),
    return_airline_name varchar(256),
    return_duration varchar(32),
    return_stops int,
    return_departure_time datetime,
    return_departure_airport varchar(256),
    return_arrival_time datetime,
    return_arrival_airport varchar(256)
);