## Flights Analyser Prototype.
___
This is a flights web scraper (cheapflights.com & skyscanner.com)

The aim was to see if being able to automate the searching of flights will help to find the cheapest flights without having to spend hours and hours fiddling with date criteria on a website.
___
### Do not try to use this for actually searching for prices
This is because the maintaining of the html structure of the websites to match to the code is not maintainable. 
It is probably out of date already.

This was purely for POC purposes.
___
### Running the POC

- Make sure you have a MySql server available.
- Make sure you have a MongoDB instance available.
- checkout the code here:
>git clone git@gitlab.com:flights-analyser/prototype-flights-price-scraper.git
- Once that is imported into your IDE open /resources/application.properties
    + Ensure the connection details are correct for the databases.
    + Ensure the MongoDB and MySQL DB instances are running
    + Run the /resources/mysql-db/create_flights_table.sql on your MySQL database
        + Make sure that the table name is the same as that configured in your application.properties
    + Run the /src/main/java/com/zorbitt/flightpriceanalyser/CheapFlightsFlightSearchRunner.java OR /src/main/java/com/zorbitt/flightpriceanalyser/SkyscannerFlightSearchRunner.java class
        + You will see it will launch an instance of Chrome and start executing the steps.
        + It will have saved all the flight information in the two databases once it is complete.
    + Now that the databases are populated you are able to run queries to aggregate the data and find the best times to fly.